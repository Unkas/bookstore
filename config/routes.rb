Rails.application.routes.draw do
  root 'main#index'

  namespace :api do
    namespace :local do
      get '/store/(:id)', to: 'store#load_store'
      get '/store_by_coords/', to: 'store#find_store_by_coords'
    end
  end  
end
