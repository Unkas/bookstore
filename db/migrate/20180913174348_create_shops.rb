class CreateShops < ActiveRecord::Migration[5.0]
  def up
    create_table :shops do |t|
      t.string :name, default: ""
      t.float :long
      t.float :lat
    end
  end

  def down
    drop_table :shops
  end
end
