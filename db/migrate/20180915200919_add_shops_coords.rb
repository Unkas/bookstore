class AddShopsCoords < ActiveRecord::Migration[5.0]
  def up
    create_table :shops_coords do |t|
      t.integer :shop_id
      t.float :lat
      t.float :long
    end
  end

  def down
    drop_table :shops_coords
  end
end
