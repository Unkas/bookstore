class CreateBooks < ActiveRecord::Migration[5.0]
  def up
    create_table :books do |t|
      t.string :title, default: ""
      t.string :author, default: ""
      t.integer :price0, default: 0
    end
  end

  def down
    drop_table :books
  end
end
