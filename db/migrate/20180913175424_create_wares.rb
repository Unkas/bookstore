class CreateWares < ActiveRecord::Migration[5.0]
  def up
    create_table :wares do |t|
      t.integer :shop_id
      t.integer :book_id
      t.integer :count, default: 0
      t.float :extra_cost
    end
  end

  def down
    drop_table :wares
  end
end
