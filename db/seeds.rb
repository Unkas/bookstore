Book.create([
  { author: 'Стивен Кинг',      title: 'Сияние',                price0: 55, id: 1 },
  { author: 'Карлос Кастанеда', title: 'Путешествие в Икстлан', price0: 70, id: 2 },
  { author: 'Гарри Гаррисон',   title: 'Стальная крыса',        price0: 40, id: 3 },
  { author: 'Харуки Мураками',  title: 'Охота на овец',         price0: 33, id: 4 },
  { author: 'Александр Носов',  title: 'Незнайка на Луне',      price0: 22, id: 5 }
])

Shop.create([
  { name: 'Main', lat: 55.70, long: 37.66, id: 1 },
  { name: 'East', lat: 55.78, long: 49.12, id: 2 },
  { name: 'West', lat: 50.45, long: 30.54, id: 3 }
])

Ware.create([
  { shop_id: 1, book_id: 1, count: 5, extra_cost: 5},
  { shop_id: 1, book_id: 2, count: 5, extra_cost: 5},
  { shop_id: 1, book_id: 3, count: 5, extra_cost: 5},
  { shop_id: 1, book_id: 4, count: 5, extra_cost: 5},
  { shop_id: 1, book_id: 5, count: 5, extra_cost: 5},
  
  { shop_id: 2, book_id: 1, count: 2, extra_cost: 7},
  { shop_id: 2, book_id: 2, count: 2, extra_cost: 7},
  { shop_id: 2, book_id: 3, count: 0, extra_cost: 7},
  { shop_id: 2, book_id: 4, count: 2, extra_cost: 7},
  { shop_id: 2, book_id: 5, count: 2, extra_cost: 7},

  { shop_id: 3, book_id: 1, count: 3, extra_cost: 10},
  { shop_id: 3, book_id: 2, count: 3, extra_cost: 10},
  { shop_id: 3, book_id: 3, count: 3, extra_cost: 10},
  { shop_id: 3, book_id: 4, count: 3, extra_cost: 10},
  { shop_id: 3, book_id: 5, count: 0, extra_cost: 10},  
])

ShopsCoord.create([
  { shop_id: 1, lat: 66.66, long: 66.66, id: 1 }
])