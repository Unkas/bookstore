FactoryGirl.define do
  factory :ware do |f|
    f.sequence(:shop_id) { |i| i }
    f.sequence(:book_id) { |i| i }
    f.count 5
    f.extra_cost 10
  end  
end