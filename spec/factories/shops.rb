FactoryGirl.define do
  factory :shop do
    sequence(:id)  { |i| i }
    sequence(:name) { |i| "Shop#{i}" }
    sequence(:lat)  { |i| i*10 }
    sequence(:long) { |i| i*10 }
  end
end