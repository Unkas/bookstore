FactoryGirl.define do
  factory :book do |f|
    f.sequence(:id)  { |i| i }
    f.sequence(:title) { |i| "Book from shop#{i}" }
    f.author "test author"
    f.sequence(:price0) { |i| i*10 }
  end  
end