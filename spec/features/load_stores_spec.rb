require 'rails_helper'
require 'spec_helper'
require 'factory_girl'


RSpec.feature "LoadStores", type: :feature do

  scenario "visit homepage" do
    FactoryGirl.create_list(:shop, 3)
    FactoryGirl.create_list(:book, 3)
    FactoryGirl.create_list(:ware, 3)
    visit root_path
    find('select').click
    find("option[value='3']").click
    expect(page).to have_text 'Book from shop3'
  end
end
