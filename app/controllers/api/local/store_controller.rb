class Api::Local::StoreController < ApplicationController

  def load_store
    @wares = Ware.in_stock.where(shop_id: params[:id])
    @wares.joins(:books).order('books.author')
    render layout: false
  end

  def find_store_by_coords
    lat = params[:lat].to_f
    long = params[:long].to_f
    db_shop = ShopsCoord.find_by(lat: lat, long: long)
    if db_shop.nil?
      nearest_shop = find_store(lat, long)
      db_shop_new = ShopsCoord.new({shop_id: nearest_shop.id, lat: lat, long: long})
      db_shop_new.save
      return 
      render json: { shop_id: db_shop_new.id }
    else
      render json: { shop_id: db_shop.id }
    end    
  end

  private

  def find_store lat, long
    nearest_shop = nil
    c_min = 40500
    Shop.all.each do |shop|
      long_diff = [shop.long - long, 180].min
      a_sq =  (shop.lat - lat) ** 2
      b_sq =  long_diff ** 2
      c_sq = a_sq + b_sq
      c = Math.sqrt(c_sq)
      if c < c_min
        nearest_shop = shop
      end
    end
    return nearest_shop
  end

end
