$( document ).ready(function() {

if(navigator.geolocation) {
  navigator.geolocation.getCurrentPosition(function(position) {
    var latitude = position.coords.latitude.toFixed(2);
    var longitude = position.coords.longitude.toFixed(2);
    console.log(latitude);
    console.log(longitude);
    findStoreByCoords(latitude, longitude)
  });
} else {
  loadBooksForStore(1);
}




$('select').click(function(e){
  loadBooksForStore(e.target.value);
});


  function findStoreByCoords(lat, long) {
    $.ajax({
      url: "/api/local/store_by_coords/",
      method: "get",
      dataType: "json",
      data: {lat: lat, long: long},
      success: function(data) {
        console.log('success');
        console.log('data', data);
        str = 'option[value=' + data.shop_id + ']'
        $(str)[0].selected = true
        loadBooksForStore(data.shop_id);
      },
      error: function(xhr, status, err) {
        console.log("error");
      }
    });
  }

  function loadBooksForStore(storeId) {
    $.ajax({
      url: "/api/local/store/" + storeId,
      method: "get",
      dataType: "text",
      success: function(data) {
        console.log('success');
        $('.products').html("");
        $('.products').append(data);    
      },
      error: function(xhr, status, err) {
        console.log("error");
      }
    });
  }

});