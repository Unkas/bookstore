module Api::Local::BookHelper

  def price(ware)
    (ware.book.price0 + ware.extra_cost).to_i
  end

end