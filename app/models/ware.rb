class Ware < ActiveRecord::Base

  belongs_to :shop
  belongs_to :book

  scope :in_stock, -> { where.not(count: 0) }

end